using Terraria;
using Terraria.ModLoader;

namespace ChaserMod.Buffs

{
	public class ChaserBuff : ModBuff
	{
		public override void SetDefaults()
		{
			DisplayName.SetDefault("Vehicle Pursuer");
			Description.SetDefault("Hard metal seat, with no room for drinks.");
			Main.buffNoTimeDisplay[Type] = true;
			Main.buffNoSave[Type] = true;
		}

		public override void Update(Player player, ref int buffIndex)
		{
			player.mount.SetMount(mod.MountType<Mounts.Chaser>(), player);
			player.buffTime[buffIndex] = 10;
		}
	}
}
