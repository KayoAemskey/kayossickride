using Terraria.ID;
using Terraria.ModLoader;

namespace ChaserMod.Items
{
	public class janglykeys : ModItem
	{
		public override void SetStaticDefaults()
		{
			DisplayName.SetDefault("janglykeys");
			Tooltip.SetDefault("Where the hell did you get these...?");
		}
		public override void SetDefaults()
		{
			item.width = 40;
			item.height = 40;
			item.useTime = 20;
			item.useAnimation = 20;
			item.useStyle = 1;
			item.knockBack = 6;
			item.value = 10000;
			item.rare = 2;
			item.UseSound = SoundID.Item1;
			item.noMelee = true;
			item.mountType = mod.MountType("Chaser");
		}

		public override void AddRecipes()
		{
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.DirtBlock, 10);
			recipe.AddTile(TileID.WorkBenches);
			recipe.SetResult(this);
			recipe.AddRecipe();
		}
	}
}
