﻿using System;
using Terraria;
using Terraria.ModLoader;

namespace ChaserMod
{
    class ModdedPlayer : ModPlayer
    {
        
        bool mountReplaced = false;        
        Mount replacedMount;

        

        //Run immediately before the HorizontalMovement method
        public override void PostUpdateRunSpeeds()
        {
            
            //Check if they even have a mount first
            if (player.mount.Active)
            {
                //Check if their mount is the right type. Separate iffs just in case they don't have a mount, not sure if that would throw an access exception or not. Better safe than whatever
                if (player.mount.BuffType == mod.BuffType("ChaserBuff"))
                {
                    mountReplaced = true;
                    replacedMount = player.mount;
                    player.mount = GenerateDummyMount(player.mount);
                    
                }

            }
        }

        private Mount GenerateDummyMount(Mount mount)
        {
            //Currently just setting the type 
            Mount dummyMount = new Mount
            {                
                _type = 14,                
            
            };

            
            return dummyMount;
        }

        //Run after the HorizontalMovement method, but some point before the graphical update. Hopefully will avoid graphical glitches.
        public override void PreUpdateMovement()
        {
            if (mountReplaced)
            {
                player.mount = replacedMount;
                mountReplaced = false;
            }
        }
        
    }
}
