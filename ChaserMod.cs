using Terraria.ModLoader;

namespace ChaserMod
{
	class ChaserMod : Mod
	{
		public ChaserMod()
		{
            Properties = new ModProperties()
            {
                Autoload = true,
                AutoloadGores = true,
                AutoloadSounds = true,                
			};
		}
	}
}
